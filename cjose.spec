Name:                cjose
Version:             0.6.2.3
Release:             1
Summary:             C library implementing the Javascript Object Signing and Encryption (JOSE)
License:             MIT
URL:                 https://github.com/OpenIDC/cjose
Source0:             https://github.com/OpenIDC/cjose/releases/download/v%{version}/%{name}-%{version}.tar.gz
BuildRequires:       gcc doxygen libtcnative-1-0 jansson-devel check-devel openssl-devel
%description
Implementation of JOSE for C/C++

%package             devel
Summary:             Development files for %{name}
Requires:            %{name}%{?_isa} = %{version}-%{release}
%description         devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install
find %{buildroot} -name '*.a' -exec rm -f {} ';'
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%check
make check || (cat test/test-suite.log; exit 1)

%files
%license LICENSE
%doc CHANGELOG.md README.md
%doc /usr/share/doc/cjose
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/cjose.pc

%changelog
* Mon Jan 06 2025 Suyun <ziyu.oerv@isrc.iscas.ac.cn> - 0.6.2.3-1
- Update to 0.6.2.3 to fix Clang build errors

* Mon Jul 24 2023 yaoxin <yao_xin001@hoperun.com> - 0.6.2.2-1
- Update to 0.6.2.2 for fix CVE-2023-37464

* Fri Feb 03 2023 xu_ping <xuping33@h-partners.com> - 0.6.1-4
- Fix build failure due to openssl upgrade 3.0

* Sat Jul 18 2020 yanan li <liyanan032@huawei.com> - 0.6.1-3
- Package init
